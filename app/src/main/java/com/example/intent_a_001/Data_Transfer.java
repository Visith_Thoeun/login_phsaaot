package com.example.intent_a_001;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Data_Transfer extends AppCompatActivity {
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_transfer);


        text = (TextView) findViewById(R.id.message1);
        Button Get = (Button)findViewById(R.id.mr_mess);

        View.OnClickListener go_to_message = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent GM = new Intent(getApplicationContext(),return_data.class);
                GM.putExtra("default", text.getText().toString());
                startActivityForResult(GM,111);
            }
        };
        Get.setOnClickListener(go_to_message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==111){
            String msg = data.getStringExtra("msg");
            text.setText(msg);
        }
    }
}