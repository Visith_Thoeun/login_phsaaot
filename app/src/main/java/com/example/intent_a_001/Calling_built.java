package com.example.intent_a_001;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Calling_built extends AppCompatActivity {
    Button btn0, btn1, btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn_star,btn_sharp;

    Button btn_del, btn_call;

    EditText edt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling_built);

        btn0 = (Button)findViewById(R.id.button0);
        btn1 = (Button)findViewById(R.id.button1);
        btn2 = (Button)findViewById(R.id.button2);
        btn3 = (Button)findViewById(R.id.button3);
        btn4 = (Button)findViewById(R.id.button4);
        btn5 = (Button)findViewById(R.id.button5);
        btn6 = (Button)findViewById(R.id.button6);
        btn7 = (Button)findViewById(R.id.button7);
        btn8 = (Button)findViewById(R.id.button8);
        btn9 = (Button)findViewById(R.id.button9);
        btn_star = (Button)findViewById(R.id.star);
        btn_sharp = (Button)findViewById(R.id.sharp);
        edt = (EditText)findViewById(R.id.edt1);

        btn_del = (Button) findViewById(R.id.del);
        btn_call = (Button) findViewById(R.id.call);


        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String remain = edt.getText().toString();
                if(remain.length()>0) {
                    remain = remain.substring(0, remain.length() - 1);
                    edt.setText(remain);
                }
            }
        });
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            String number = "tel:" + edt.getText().toString();
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(number)));
            }
        });
    }
    public void myNumber (View view){
        Button btn = (Button)view;
        String str = (String)btn.getText();
        edt.setText(edt.getText()+str);
    }
}