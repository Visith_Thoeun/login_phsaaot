package com.example.intent_a_001;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class return_data extends AppCompatActivity {
    EditText edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_data);

        edit = (EditText)findViewById(R.id.return_mess);
        Button submit = (Button)findViewById(R.id.sup_mess);

        Intent intent = getIntent();
        String msg = intent.getStringExtra("default");
        edit.setText(msg);

        View.OnClickListener msg1 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = edit.getText().toString();
                Intent rs = new Intent();
                rs.putExtra("msg", str);
                setResult(111, rs);
                finish();
            }
        };
        submit.setOnClickListener(msg1);
    }
}