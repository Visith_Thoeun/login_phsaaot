package com.example.intent_a_001;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //go to webview.
        View.OnClickListener go_to_webview = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), web_views.class);
                startActivity(i);
            }
        };
        Button btn_webview = (Button)findViewById(R.id.go_http);
        btn_webview.setOnClickListener(go_to_webview);


        //Calling built-in
        View.OnClickListener go_to_Calling_built = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Call = new Intent(getApplicationContext(), Calling_built.class);
                startActivity(Call);
            }
        };
        Button btn_call  = (Button)findViewById(R.id.go_calling);
        btn_call.setOnClickListener(go_to_Calling_built);

        //Data Transfer
        View.OnClickListener go_to_Data_Transfer = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Transfer = new Intent(getApplicationContext(),Data_Transfer.class);
                startActivity(Transfer);
            }
        };
        Button Data_Transfer = (Button)findViewById(R.id.go_login);
        Data_Transfer.setOnClickListener(go_to_Data_Transfer);


    }
}