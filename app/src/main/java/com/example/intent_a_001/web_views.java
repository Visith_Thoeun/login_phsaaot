package com.example.intent_a_001;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class web_views extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_views);
        //Create webview.
       WebView http_view =findViewById(R.id.mr_view);


        //call url

        http_view.loadUrl("https://www.facebook.com/");
    }
}